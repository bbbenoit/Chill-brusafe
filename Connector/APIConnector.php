<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\BrusafeBundle\Connector;

//use GuzzleHttp\Client as GuzzleHttpClient;
use Psr\Log\LoggerInterface;
use GuzzleHttp\Psr7;
use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


/**
 * Handle connection with api portal
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class APIConnector
{
    private $location;
    
    private $redirectUri = 'http://localhost:8000/brusafe/connect_callback';
    
    private $clientId;
    
    /**
     *
     * @var RequestStack
     */
    protected $requestStack;
    
    /**
     *
     * @var UrlGeneratorInterface 
     */
    protected $urlGenerator;
    
    /**
     *
     * @var LoggerInterface
     */
    protected $logger;
    
    /**
     *
     * @var string
     */
    protected $authenticationPath;
    
    /**
     *
     * @var string 
     */
    protected $tokenPath;
    
    /**
     * caching the therapeutic link, to avoid multiple identical request
     * during same current Request
     * 
     * @var string[]
     */
    private $therapeuticLinkStateCache = array();
    
    /**
     * cache the therapeutic link assertion, to avoid multiple identical
     * request during current Request
     *
     * @var string[]
     */
    private $therapeuticLinkAssertionCache = array();
    
    /**
     * path to store access token in session
     */
    const BRUSAFE_API_BEARER = 'brusafe_api_bearer';
    
    /**
     * path to store expiration token, as an unix timestamp, in session
     */
    const BRUSAFE_API_BEARER_EXPIRATION = 'brusafe_api_bearer_expiration';
    
    /**
     * path to store state, in session
     */
    const BRUSAFE_API_AUTH_STATE = 'brusafe_api_auth_state';
    
    /**
     * path to get the state of the medical relation
     *
     * @var string
     */
    private $BRUSAFE_PATH_STATE = 'medical/relation/state';
    
    /**
     * Create a therapeutical relation using the code that 
     * was sent to the patient.
     *
     * @var string
     */
    private $BRUSAFE_PATH_CONFIRM = 'medical/relation/confirm';
    
    /**
     * Get the assertion that proves there is a relationship 
     * between therapeutist and patient.
     *
     * @var string
     */
    private $BRUSAFE_PATH_GET = 'medical/relation/assertion';
    
    /**
     * path to request a medical relation
     * 
     * @var string 
     */
    private $BRUSAFE_PATH_REQUEST = 'medical/relation/request';
    
    /**
     * the message used in logging when the authentication 
     * failed.
     *
     * @var string
     */
    private static $authenticationFailedMsg = "Authentication to ABRUMET api "
        . "failed";
    
    public function __construct(
        LoggerInterface $logger,
        RequestStack $requestStack,
        Router $router
    ) {
        $this->location     = 'https://auth.qa.brusafe.be';
        $this->clientId     = 'mass';
        $this->urlGenerator = $router->getGenerator();
        $this->logger       = $logger;
        $this->requestStack = $requestStack;
        
        $this->authenticationPath = '/auth/realms/abrumet/protocol/'
            . 'openid-connect/auth';
        $this->tokenPath          = '/auth/realms/abrumet/protocol/'
            . 'openid-connect/token';
    }
    
    /**
     * get the state for the current session.
     * 
     * If not set, create this state.
     * 
     * @return string
     */
    protected function getState()
    {
        $session = $this->requestStack->getCurrentRequest()->getSession();
        
        if (! $session->has(self::BRUSAFE_API_AUTH_STATE)) {
            $session->set(self::BRUSAFE_API_AUTH_STATE, \uniqid(rand(), true));
        }
        
        return $session->get(self::BRUSAFE_API_AUTH_STATE);
    }
    
    protected function getRedirectUri()
    {
        return $this->urlGenerator->generate('chill_brusafe_connect_callback',
            [], UrlGeneratorInterface::ABSOLUTE_URL);
    }
    
    public function isAuthenticated()
    {
        $session = $this->requestStack->getCurrentRequest()->getSession();
        
        return 
            $session->get(self::BRUSAFE_API_BEARER, null) !== null
            AND
            (int) $session->get(self::BRUSAFE_API_BEARER_EXPIRATION) > \time();
    }
    
    public function generateAuthenticationLink($relativeRedirection = null)
    {
        $parameters = array(
            'response_type' => 'code',
            'client_id'     => $this->clientId,
            'redirect_uri'  => $this->getRedirectUri(),
            /* commented until we have the correct callback
             * $this->urlGenerator
                ->generate(
                    'chill_brusafe_connect_callback', 
                    [],
                    UrlGeneratorInterface::ABSOLUTE_URL
                    ),*/
            'nonce'         => '0',
            'state'         => $this->getState()
        );
        
        return $this->location.
            $this->authenticationPath
            .'?'
            . \http_build_query($parameters);
    }
    
    public function authenticate()
    {
        $request = $this->requestStack->getCurrentRequest();
        list($code, $state) = $this->getAuthenticationParameters();
        
        if ($state !== $this->getState()) {
            $msg = "The returned state is different from the expected state";
            
            $this->logger->error($msg, [ 'expected' => $this->getState(), 
                'returned' => $state]);
            
            throw new APIAuthenticationException($msg);
        }
        
        list($accessToken, $idToken, $refreshToken, $expiresIn, 
            $refreshTokenExpiresIn, $tokenType) = $this->processCode($code);
        
        if ($tokenType !== 'bearer') {
            throw new APIAuthenticationException(sprintf("The id_token is not "
                . "a bearer, %s given", $tokenType));
        }
        
        // will throw an APIAuthenticationException if not valid
        $this->validateIdToken($accessToken);
        
        $expiration = \time() + (int) $expiresIn;
        
        $request->getSession()->set(self::BRUSAFE_API_BEARER, $accessToken);
        $request->getSession()->set(self::BRUSAFE_API_BEARER_EXPIRATION, 
            $expiration);
        
        
        list($name, $preferredUsername) = $this->parseAccessToken($idToken);
        
        $this->logger->info("An user was authenticated against brusafe+ portal",
            [
                'expiration' => $expiration,
                'name'=> $name, 
                'preferred_username' => $preferredUsername
            ]
        );
        
        return $name;
    }
    
    protected function validateIdToken($idToken)
    {
        // TODO
    }
    
    /**
     * parse the access token to get the name
     * and preferrend username
     * 
     * @param string $accessToken
     * @return string[] name, preferred_username
     */
    protected function parseAccessToken($accessToken)
    {
        list($alg, $content, $sig) = \explode('.', $accessToken);
        
        $parsed = \json_decode(\base64_decode($content));
        
        return [$parsed->name, $parsed->preferred_username];
    }
    
    /**
     * process the code to get an access token, token id, ..., 
     * 
     * @param string $code
     * @return string[] in this order : 'access_token', 'id_token', 'refresh_token', 'expires_in',
            'refresh_expires_in', 'token_type'
     * @throws APIAuthenticationException
     */
    private function processCode($code)
    {
        $client = new \GuzzleHttp\Client([ 'base_uri' => $this->location ]);
        
        $body = array(
            'grant_type'   => 'authorization_code',
            'code'         => $code,
            'redirect_uri' => $this->getRedirectUri(),
            'client_id'    => $this->clientId,
            'client_secret'=> '93ab6691-299d-4e69-ab6b-ecbbaf645713'
            );
        
        try {
            $response = $client->request('POST', $this->tokenPath, 
                [ 
                    'form_params' => $body
                ]);
        } catch (RequestException $ex) {
            $parsed = \json_decode($ex->getResponse()->getBody());
            
            $this->logger->error("Bad request for token", [
                'request' => Psr7\str($ex->getRequest()),
                'response' => Psr7\str($ex->getResponse()),
                'error'   => $parsed->error,
                'error_description' => $parsed->error_description
            ]);
            
            throw new APIAuthenticationException(
                sprintf(
                    "Bad request for token : %s, %s",
                    $parsed->error,
                    $parsed->error_description
                ));
        }
        
        // parse the response and return as an array every element
        $parsed = \json_decode($response->getBody());
        
        foreach (['access_token', 'id_token', 'refresh_token', 'expires_in',
            'refresh_expires_in', 'token_type'] as $v) {
            
            if (!isset($parsed->{$v}) or empty($parsed->{$v})) {
                $msg = sprintf("Invalid request: %s should be set", $v);
                $this->logger->warning(self::$authenticationFailedMsg, 
                    ["description" => $msg]);
            
                throw new APIAuthenticationException($msg);
            }
            
            $result[] = $parsed->{$v};
        }
        
        return $result;
    }
    
    /**
     * Parse the query parameters returned by the authentication endpoint
     * and get the parameters necessary for authentication.
     * 
     * This function extract the 'code' and 'state' from the query string
     * (`code=eyf...&state=123`)
     * 
     * @return string[2] $code, $state
     * @throws APIAuthenticationException if the parameters are not valid, or
     *   \ if the parameters are an error
     */
    private function getAuthenticationParameters()
    {
        $request = $this->requestStack->getCurrentRequest();
        // handle errors
        $error = $request->query->get('error', null);
        $errorDescription = $request->query->get('error_description', '');
        
        if ($error !== null) {
            $this->logger->warning(self::$authenticationFailedMsg,
                array('error' => $error, 'description' => $errorDescription));
            
            throw new APIAuthenticationException(\urldecode($errorDescription));
        }
        
        $code  = $request->query->get('code' , null);
        $state = $request->query->get('state', null);
        
        foreach (['state', 'code'] as $v) {
            if (${$v} === null) {
                $msg = sprintf("Invalid request: %s should not be null", $v);
                $this->logger->warning(self::$authenticationFailedMsg, 
                    ["description" => $msg]);
            
                throw new APIAuthenticationException($msg);
            }
        }
        
        return [$code, $state];
    }
    
    /**
     * create a client configured to `location`
     * 
     * @return \GuzzleHttp\Client
     */
    protected function createApiClient()
    {
        return new \GuzzleHttp\Client([
            'time_out' => '2.0',
            'base_uri' => $this->location.'/relation-api/api/v1/',
            'headers'  => array(
                'Authorization' => 'Bearer '.$this->requestStack
                    ->getCurrentRequest()
                    ->getSession()
                    ->get(self::BRUSAFE_API_BEARER)
            )
        ]);
    }
    
    /**
     * get the therapeutic link state
     * 
     * @param string $niss
     * @return string
     * @throws ApiError if the api return an error 400 (malformed)
     * @throws APIAuthenticationException if the api return an error > 400 and < 500
     * @throws \RuntimeException if the error is >= 500
     */
    public function getTherapeuticLinkState($niss)
    {
        // return from the cache if exists
        if (array_key_exists($niss, $this->therapeuticLinkStateCache)) {
            return $this->therapeuticLinkStateCache[$niss];
        }
        
        $client = $this->createApiClient();
        
        try {
            /* @var $response \Psr\Http\Message\ResponseInterface */
            $response = $client->get($this->BRUSAFE_PATH_STATE, array(
                'query' => [ 'national-registry-number' => $niss ]
            ));
        } catch (RequestException $ex) {
            if ($ex->getCode() === 400) {
                $this->logger->warning("An error occured while retrieving medical "
                    . "consent", 
                    array(
                        'national-registry-number' => $niss,
                        'errorType' => $parsed->errorType,
                        'errorMessage' => $parsed->message
                    ));

                throw new ApiError($parsed->errorType, $parsed->message);
            } elseif ($ex->getCode() > 400 AND $ex->getCode() < 500) {
                
                $parsed = \json_decode($ex->getResponse()->getBody());
                dump($parsed);
                dump($ex->getResponse()->getBody());
                
                $this->logger->warning("A connection to api was attempted, "
                    . "but returned an error between 400 and 500", array(
                        'errorCode' => $ex->getCode(),
                        'errorType' => null === $parsed ? 'not available' : $parsed->errorType,
                        'message'   => null === $parsed ? 'not available' : $parsed->message
                    ));
                // do more logging during debug
                $this->logger->debug("Request with portal tracer",
                    array(
                        'request' => Psr7\str($ex->getRequest()),
                        'response' => Psr7\str($ex->getResponse())
                    ));

                throw new APIAuthenticationException("The connection to brusafe "
                    . "failed");
            } else {
                $msg = sprintf("The request to brusafe api returned an unexpected "
                    . "exception");

                $this->logger->error($msg, array(
                    'code' => $ex->getCode(),
                    'content' => $ex->getMessage(),
                    'niss' => $niss
                ));

                throw new \RuntimeException($msg, 0, $ex);
            }
        }
        
        $parsed = \json_decode($response->getBody());
        
        //if ($response->getStatusCode() === 200) {
            $this->therapeuticLinkStateCache[$niss] = $parsed->state;
            
            return $parsed->state;
            
        /*} else {
            $msg = "The request to brusafe api returned an unexpected code";
            
            $this->logger->error($msg, array(
                'code' => $response->getStatusCode(),
                'body' => $response->getBody()
            ));
            
            throw new \RuntimeException(sprintf($msg." : %s", 
                $response->getStatusCode()));
        }*/
    }
    
    /**
     * request a therapeutic link
     * 
     * @param string $niss
     * @throws ApiError if the request returned an error
     */
    public function requestTherapeuticLink($niss)
    {
        $this->logger->debug('request therapeutic link', [ 'niss' => $niss ] );
        
        $client = $this->createApiClient();
        
        try {
            $client->request('PUT', $this->BRUSAFE_PATH_REQUEST, [
                'query' => [ 'national-registry-number' => $niss ]
            ]);
        } catch (RequestException $ex) {
            $parsed = \json_decode($ex->getResponse()->getBody());
            
            if ($ex->getResponse()->getStatusCode() === 400) {
                $this->logger->warning("Error 400 during a request of therapeutic "
                    . "link",
                    array(
                        'errorType' => $parsed->errorType,
                        'message'   => $parsed->message
                    ));

                throw new ApiError($parsed->errorType, $parsed->message);
            } elseif ($ex->getResponse()->getStatusCode() < 500) {
                $msg = "Error btw 401 and 499 during a request of therapeutic "
                    . "link";
                $this->logger->warning($msg,
                    array(
                        'response' => Psr7\str($ex->getResponse())
                    ));
                
                throw new APIAuthenticationException("The connection to brusafe "
                    . "failed");
            } else {
                $msg = "Unknown error during request of a therapeutic link";
                $this->logger->warning($msg,
                    array(
                        'response' => Psr7\str($ex->getResponse())
                    ));
                
                throw new \RuntimeException($msg, 0, $ex);
            }
        }
    }
    
    public function confirmTherapeuticLink($niss, $code)
    {
        $this->logger->debug('confirm therapeutic link', [ 'niss' => $niss ] );
        
        $client = $this->createApiClient();
        
        try {
            $client->request('PUT', $this->BRUSAFE_PATH_CONFIRM, [
                'query' => [ 
                    'national-registry-number' => $niss,
                    'code' => $code
                ]
            ]);
        } catch (RequestException $ex) {

            if ($ex->getResponse()->getStatusCode() === 400) {
                
                $parsed = \json_decode($ex->getResponse()->getBody());
                $this->logger->warning("Error during a confirmation of therapeutic link",
                    array(
                        'errorType' => $parsed->errorType,
                        'message'   => $parsed->message,
                        'http_code' => $ex->getResponse()->getStatusCode(),
                        'response'  => Psr7\str($ex->getResponse())
                    ));
                // add more info in debug
                $this->logger->debug("Error during a confirmation of therapeutic link",
                    array(
                        'request' => Psr7\str($ex->getRequest())
                    ));

                throw new APIError($parsed->errorType, $parsed->message);
                
            } elseif ($ex->getResponse()->getStatusCode() > 400 
                AND $ex->getResponse()->getStatusCode() < 500) {
                
                $this->logger->warning("Error during a confirmation of therapeutic link",
                    array(
                        'http_code' => $ex->getResponse()->getStatusCode(),
                        'response'  => Psr7\str($ex->getResponse())
                    ));
                
                $this->logger->debug("Error during a confirmation of therapeutic link",
                    array(
                        'request' => Psr7\str($ex->getRequest())
                    ));
                
                throw new APIAuthenticationException("Error during a "
                    . "confirmation of therapeutic link");
                
            } else {
                $msg = "Unknown error during request of a therapeutic link";
                $this->logger->warning($msg,
                    array(
                        'response' => Psr7\str($ex->getResponse())
                    ));
                
                throw new \RuntimeException($msg, 0, $ex);
            }
            
            
        }
    }
    
    public function getTherapeuticLinkAssertion($niss)
    {
        $this->logger->debug('get therapeutic link', [ 'niss' => $niss ] );
        
        $client = $this->createApiClient();
        
        try {
            $response = $client->request('GET', $this->BRUSAFE_PATH_GET, [
                'query' => [ 
                    'national-registry-number' => $niss
                ]
            ]);
        } catch (RequestException $ex) {

            if ($ex->getResponse()->getStatusCode() === 400) {
                
                $parsed = \json_decode($ex->getResponse()->getBody());
                $this->logger->warning("Error during a confirmation of therapeutic link",
                    array(
                        'errorType' => $parsed->errorType,
                        'message'   => $parsed->message,
                        'http_code' => $ex->getResponse()->getStatusCode(),
                        'response'  => Psr7\str($ex->getResponse())
                    ));
                // add more info in debug
                $this->logger->debug("Error during a confirmation of therapeutic link",
                    array(
                        'request' => Psr7\str($ex->getRequest())
                    ));

                throw new APIError($parsed->errorType, $parsed->message);
                
            } elseif ($ex->getResponse()->getStatusCode() > 400 
                AND $ex->getResponse()->getStatusCode() < 500) {
                
                $this->logger->warning("Error during a confirmation of therapeutic link",
                    array(
                        'http_code' => $ex->getResponse()->getStatusCode(),
                        'response'  => Psr7\str($ex->getResponse())
                    ));
                
                $this->logger->debug("Error during a confirmation of therapeutic link",
                    array(
                        'request' => Psr7\str($ex->getRequest())
                    ));
                
                throw new APIAuthenticationException("Error during a "
                    . "confirmation of therapeutic link");
                
            } else {
                $msg = "Unknown error during request of a therapeutic link";
                $this->logger->warning($msg,
                    array(
                        'response' => Psr7\str($ex->getResponse())
                    ));
                
                throw new \RuntimeException($msg, 0, $ex);
            } 
        }
        
        $xml = $response->getBody();
        
        $this->therapeuticLinkAssertionCache[$niss] = $xml;
        
        $this->logger->info("Retrieved a therapeutic assertion successfully "
            . "from abrumet portal");
        
        $this->logger->debug("Retrieved a therapeutic assertion successfully "
            . "from abrumet portail", [
                'xml' => $xml,
                'niss' => $niss
            ]);

        return $xml;
    }
}
