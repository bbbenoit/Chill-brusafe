<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\BrusafeBundle\Controller;

use Chill\BrusafeBundle\Form\BrusafePublicationType;
use Chill\MainBundle\Entity\Center;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Chill\HealthBundle\Entity\Consultation;
use Chill\HealthBundle\Security\Authorization\ConsultationVoter;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Chill\HealthBundle\Entity\Publication;
use Chill\HealthBundle\Security\Authorization\PublicationVoter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Chill\BrusafeBundle\Connector\TherapeuticRelation;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class PublicationController extends Controller
{
    /**
     * get a consultation and try the ACL
     * 
     * @param Request $request
     * @return Consultation
     * @throws \InvalidArgumentException if consultation_id is missing in query
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException if consultation not found
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException if the access does not have access to the consultation
     */
    private function getConsultation($consultationId)
    {
        $consultation = $this->getDoctrine()->getManager()
            ->find(Consultation::class, $consultationId);
        
        if ($consultation === null) {
            throw $this->createNotFoundException("Consultation not found");
        }
        
        $this->denyAccessUnlessGranted(ConsultationVoter::SEE, $consultation);
        
        return $consultation;
    }
    
    public function showAction($publication_id, $_format, Request $request)
    {
        /* @var $publication Publication */
        $publication = $this->getDoctrine()->getManager()
            ->find(Publication::class, $publication_id);
        
        if ($publication === null) {
            throw $this->createNotFoundException("Publication not found");
        }
        
        //$this->denyAccessUnlessGranted(PublicationVoter::SEE, $publication);
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $publication->getPatient());
        
        switch($_format) {
            case 'html':
                return $this->render('ChillBrusafeBundle:Publication:show.html.twig', [
                    'publication' => $publication
                ]);
            case 'xml':
                // create document
                $doc = new \DOMDocument('1.0');
                // add rendering instructions
                $xslt = $doc->createProcessingInstruction(
                    'xml-stylesheet', 
                    sprintf(
                        'type="text/xsl" href="%s"',
                        $request->getUriForPath('/bundles/chillbrusafe/xslt/cda-web.xsl')
                        )
                    );
                $doc->appendChild($xslt);
                
                // import node from CDA in publication->getData()
                $cda = new \DOMDocument('1.0');
                $cda->loadXML($publication->getData());
                $cdas = $cda->getElementsByTagName('ClinicalDocument');
                
                $node = $doc->importNode($cdas->item(0), true);
                $doc->appendChild($node);
                
                return (new Response($doc->saveXML()));
        }
    }
    
    public function newAction($consultation_id, Request $request)
    {
        $consultation = $this->getConsultation($consultation_id);
        
        /* @var $person \Chill\PersonBundle\Entity\Person */
        $person = $consultation->getPatient();
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        
        $publication = (new Publication())
            ->setPatient($consultation->getPatient())
            ->setAuthor($this->getUser())
            ->setPublicationType('brusafe_ref_summary')
            ->setConsultation($consultation)
            ;
        
        //$this->denyAccessUnlessGranted(PublicationVoter::CREATE, $publication);
        
        $form = $this->createCreateForm($publication, $consultation->getCenter());
        
        return $this->render('ChillBrusafeBundle:Publication:new.html.twig',
            array(
                'form'   => $form->createView(),
                'person' => $person,
                'consultation' => $consultation
            ));
    }
    
    public function createDocumentAction($consultation_id, Request $request)
    {
        $consultation = $this->getConsultation($consultation_id);
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $consultation->getPatient());
        
        $publication = (new Publication())
            ->setPatient($consultation->getPatient())
            ->setAuthor($this->getUser())
            ->setPublicationType('brusafe_ref_summary')
            ->setConsultation($consultation)
            ;
        
        $form = $this->createCreateForm($publication, $publication->getCenter());
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $publication = $form->getData();
            
            // $this->denyAccessUnlessGranted(PublicationVoter::CREATE, $publication);
            
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($publication);
            $em->flush();
            
            $this->addFlash('success', $this->get('translator')->trans('The '
                . 'publication is successfully created!'));
            
            return new Response("ok !");
        }
        $string = "";
        foreach ($form->getErrors(true) as $error) {
            $string.= $error->getMessage()." ".$error->getOrigin()->getName();
        }
        
        return new Response($string);
    }
    
    public function postDocumentAction($person_id, Request $request)
    {
        /* @var $person \Chill\PersonBundle\Entity\Person */
        $person = $this->get('chill.person.repository.person')
            ->find($person_id);
        
        if ($person === null) {
            throw $this->createNotFoundException("The person is not found");
        }
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        
        /* @var $connector \Chill\BrusafeBundle\Connector\APIConnector */
        $connector = $this->get('chill_brusafe.connector');
        
        // check that we are authenticated
        if ($connector->isAuthenticated() === false) {
            $this->addFlash('notice', $this->get('translator')
                ->trans('Connect to brusafe before posting the document'));
            
            return $this->redirectToRoute('chill_brusafe_connect', [
                'returnPath' => $request->getUri()
            ]);
        }
        
        $niss = $request->query->get('niss', null);
        
        if ($niss === null) {
            throw new \RuntimeException("the niss is not defined");
        }
        
        // check that a therapeutic link exist
        switch ($connector->getTherapeuticLinkState($niss)) {
            case TherapeuticRelation::STATE_AWAITING_APPROVAL:
                $this->addFlash('notice', $this->t("Please confirm therapeutic "
                    . "link before posting document"));
                
                return $this->redirectToRoute('chill_brusafe_link_confirm', 
                    array(
                        'returnPath' => $request->getUri(),
                        'niss'       => $niss,
                        'person_id'  => $person->getId()
                    ));
                
            case TherapeuticRelation::STATE_NONE:
                $this->addFlash('notice', $this->t("Create a therapeutic link "
                    . "before posting document"));
                
                return $this->redirectToRoute('chill_brusafe_link_new',
                    array(
                        'returnPath' => $request->getUri(),
                        'niss'       => $niss,
                        'person_id'  => $person->getId()
                    ));
                
            case TherapeuticRelation::STATE_NO_MEDICAL_DATA_SHARING_CONSENT:
                $this->addFlash('error', $this->t("No data sharing consent"));
                
                return $this->redirectToRoute('chill_main_homepage');
                
            case TherapeuticRelation::STATE_OK:
                return new Response("Ok, le document est envoyé !");
            
            default:
                throw new \UnexpectedValueException(sprintf(
                    "The returned code is unknown. Return : %s",
                    $connector->getTherapeuticLinkState($niss)
                    ));
        }
        
    }
    
    /**
     * Local function to translate message
     * 
     * @param type $message
     * @param type $params
     */
    private function t($message, $params = array())
    {
        return $this->get('translator')->trans($message, $params);
    }
    
    /**
     * 
     * @param Publication $publication
     * @param Center      $center
     * @return type
     */
    private function createCreateForm(Publication $publication, Center $center) {
        $form = $this->createForm(
            BrusafePublicationType::class, 
            $publication,
            [
                'center'       => $center,
                'role'         => new Role(PublicationVoter::CREATE),
                'action'       => $this
                    ->generateUrl(
                        'chill_brusafe_document_create',
                        [
                            'consultation_id' => $publication
                                ->getConsultation()
                                ->getId()
                        ])
            ]
        );
        
        $form->add('submit', SubmitType::class);
        
        return $form;
    }
}
