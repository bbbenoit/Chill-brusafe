<?php

namespace Chill\BrusafeBundle\Controller;

use Chill\BrusafeBundle\Connector\APIError;
use Chill\BrusafeBundle\Connector\APIAuthenticationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Chill\BrusafeBundle\Connector\TherapeuticRelation;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class ConnectController extends Controller
{
    const CONNECT_RETURN_PATH = 'brusafe_connect_controller_returnpath';
    
    /**
     * Show a page with a button to connect
     * 
     * Possible query parameters :
     * - `returnPath` : once connected, the user will be returned to the
     * url given as argument
     * 
     * @param Request $request
     * @return Response|RedirectResponse
     */
    public function connectAction(Request $request)
    {
        /* @var $connector \Chill\BrusafeBundle\Connector\APIConnector */
        $connector = $this->get('chill_brusafe.connector');
        
        // store redirection
        $request->getSession()->set(self::CONNECT_RETURN_PATH, 
            $request->query->get('returnPath', null));
        
        if ($connector->isAuthenticated($request) === true) {
            if (null !== $request->getSession()
                ->get(self::CONNECT_RETURN_PATH, null)) {
                
                return new RedirectResponse($request->getSession()
                    ->get(self::CONNECT_RETURN_PATH));
            } else {
                
                $this->addFlash('notice', $this->translator()->trans('You are '
                    . 'already authenticated against brusafe+'));
                
                return $this->redirectToRoute('chill_main_homepage');
            }
            
            return new Response("you are connected");
            
        }
        
        $link = $connector->generateAuthenticationLink();
        
        
        return $this->render('ChillBrusafeBundle:Connect:connect.html.twig',
            ['link' => $link]);
    }

    /**
     * Receive parameters from authentication endpoint, and launch operation
     * to retains authentication against endpoint
     * 
     * @param Request $request
     * @return RedirectResponse
     */
    public function connectCallbackAction(Request $request)
    {
        /* @var $connector \Chill\BrusafeBundle\Connector\APIConnector */
        $connector = $this->get('chill_brusafe.connector');
        
        try {
            $name = $connector->authenticate($request);
        } catch (APIAuthenticationException $ex) {
            $this->addFlash('error', $this->get('translator')
                ->trans($ex->getMessage()));
            
            return $this->redirectToRoute('chill_brusafe_connect');
        }
        
        $returnPath = $request->getSession()
            ->get(self::CONNECT_RETURN_PATH, null);
        
        if ($returnPath !== null) {
            return new RedirectResponse($returnPath);
        } else {
            return $this->redirectToRoute('chill_main_homepage');
        }
    }
    
    public function createTherapeuticLinkAction($person_id, Request $request)
    {
        /* @var $person \Chill\PersonBundle\Entity\Person */
        $person = $this->get('chill.person.repository.person')
            ->find($person_id);
        
        if ($person === null) {
            throw $this->createNotFoundException("The person is not found");
        }
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        
        /* @var $connector \Chill\BrusafeBundle\Connector\APIConnector */
        $connector = $this->get('chill_brusafe.connector');
        
        $niss = $request->query->get('niss', null);
        
        if ($niss === null) {
            throw new \RuntimeException("The niss is not set");
        }
        
        try {
            $connector->isAuthenticated();
        } catch (APIAuthenticationException $ex) {
            return $this->redirectToRoute('chill_brusafe_connect', array(
                'returnPath' => $request->getUri()
            ));
        }
        
        try {
            $state = $connector->getTherapeuticLinkState($niss);
        } catch (APIAuthenticationException $ex) {
            $this->get('logger')->error($ex->getMessage());
            
            return $this->redirectToRoute('chill_brusafe_connect', array(
                'returnPath' => $request->getUri()
            ));
        } catch (APIError $ex) {
            return $this->render('ChillBrusafeBundle:Connect:'
                . 'link_error.html.twig',
                    array(
                        'person' => $person,
                        'errorType' => $ex->getErrorType(),
                        'message' => $ex->getMessage()
                    ));
        }
        
        switch ($state) { 
            
            case TherapeuticRelation::STATE_OK:
                return new \Symfony\Component\HttpFoundation\Response(
                    "the therapeutic link exists");
                
            case TherapeuticRelation::STATE_AWAITING_APPROVAL:
            
                $this->addFlash('notice', $this->get('translator')->trans("A "
                    . "therapeutic link is already awaiting approval."));
            
                return $this->redirectToRoute('chill_brusafe_link_confirm', 
                        array(
                            'person_id' => $person->getId(),
                            'niss' => $niss,
                            'returnPath' => $request->query->get('returnPath', null)
                        ));
            case TherapeuticRelation::STATE_NO_MEDICAL_DATA_SHARING_CONSENT:
            
                return $this->render('ChillBrusafeBundle:Connect:'
                    . 'therapeutic_link_error_no_medical_data.html.twig',
                    array(
                        'person' => $person
                    ));
            case TherapeuticRelation::STATE_NONE:
                
                // request a link
                try {
                    $connector->requestTherapeuticLink($niss);

                    return $this->redirectToRoute('chill_brusafe_link_confirm', 
                        array(
                            'person_id' => $person->getId(),
                            'niss' => $niss,
                            'returnPath' => $request->query->get('returnPath', null)
                        ));

                } catch (APIError $ex) {
                    return $this->render('ChillBrusafeBundle:Connect:'
                        . 'link_error.html.twig',
                        array(
                            'person' => $person,
                            'errorType' => $ex->getErrorType(),
                            'message' => $ex->getMessage()
                        ));
                }
        }
    }
    
    /**
     * 
     * possible parameters:
     * 
     * - `niss` : the niss of the person
     * - `returnPath` : the return path after confirmation
     * 
     * @param type $person_id
     * @param Request $request
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException if person does not exists
     */
    public function newTherapeuticLinkAction($person_id, Request $request)
    {
        /* @var $person \Chill\PersonBundle\Entity\Person */
        $person = $this->get('chill.person.repository.person')
            ->find($person_id);
        
        if ($person === null) {
            throw $this->createNotFoundException("The person is not found");
        }
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        
        /* @var $connector \Chill\BrusafeBundle\Connector\APIConnector */
        $connector = $this->get('chill_brusafe.connector');
        
        $niss = $request->query->get('niss', '');
        
        // check that the user is authenticated against brusafe
        if ($connector->isAuthenticated($request) === false) {
            $this->redirectToRoute('chill_brusafe_connect', 
                array(
                    'returnPath' => $this->generateUrl(
                        'chill_brusafe_link_new', 
                        array(
                            'person_id' => $person->getId(),
                            'niss'      => $niss,
                            'returnPath' => $request->query->get('returnPath', null)
                        ), 
                        UrlGeneratorInterface::ABSOLUTE_URL
                        )
                ));
        }
        
        $form = $this->createPatientForm(
            $this->generateUrl(
                'chill_brusafe_link_create', 
                [
                    'person_id' => $person->getId()
                ]
                ), 
            $niss,
            $returnPath = $request->query->get('returnPath', null)
            );
        
        return $this->render('ChillBrusafeBundle:Connect:link_new.html.twig',
            array('form' => $form->createView(), 'person' => $person)
            );
    }
    
    /**
     * 
     * @param type $person_id
     * @param Request $request
     * @return Response
     * @throws type
     * @throws \RuntimeException
     */
    public function confirmTherapeuticLinkAction($person_id, Request $request)
    {
        /* @var $person \Chill\PersonBundle\Entity\Person */
        $person = $this->get('chill.person.repository.person')
            ->find($person_id);
        
        if ($person === null) {
            throw $this->createNotFoundException("The person is not found");
        }
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        
        $niss = $request->query->get('niss', '');
        
        if ($niss === null) {
            throw new \RuntimeException("The niss should be defined");
        }
        
        $form = $this->createConfirmForm($niss, $person, 
            $request->get('returnPath', null));
        
        return $this->render('ChillBrusafeBundle:Connect:link_confirm.html.twig',
            array(
                'person' => $person,
                'form' => $form->createView()
            ));
    }
    
    public function confirmCodeTherapeuticLinkAction($person_id, Request $request)
    {
        /* @var $person \Chill\PersonBundle\Entity\Person */
        $person = $this->get('chill.person.repository.person')
            ->find($person_id);
        
        if ($person === null) {
            throw $this->createNotFoundException("The person is not found");
        }
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        
        /* @var $connector \Chill\BrusafeBundle\Connector\APIConnector */
        $connector = $this->get('chill_brusafe.connector');
        
        $niss = $request->query->get('niss', '');
        
        if ($niss === null) {
            throw new \RuntimeException("The niss should be defined");
        }
        
        $form = $this->createConfirmForm($niss, $person);
        
        // test that the connection is still valid
        if ($connector->isAuthenticated() === false) {
            return $this->redirectToRoute('chill_brusafe_connect', array(
                'returnPath' => $this->generateUrl('chill_brusafe_link_confirm',
                    [
                        'person_id' => $person->getId(), 
                        'niss' => $niss,
                        'returnPath' => $request->query->get('returnPath', null)
                    ])
            ));
        }
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            // confirm the link
            try {
                $connector->confirmTherapeuticLink($niss, $form['code']->getData());
                
                $this->addFlash('success', $this->get('translator')
                    ->trans('A new link is created with patient'));
                
                if ($request->query->has('returnPath')) {
                    return $this->redirect($request->query->get('returnPath'));
                } else {
                    return $this->redirectToRoute('chill_main_homepage');
                }
                
            } catch (APIError $ex) {
                return $this->render('ChillBrusafeBundle:Connect:'
                        . 'link_error.html.twig',
                        array(
                            'person' => $person,
                            'errorType' => $ex->getErrorType(),
                            'message' => $ex->getMessage()
                        ));
            } catch (APIAuthenticationException $ex) {
                return $this->redirectToRoute('chill_brusafe_connect', array(
                    'returnPath' => $this->generateUrl('chill_brusafe_link_confirm',
                        [
                            'person_id' => $person->getId(), 
                            'niss' => $niss,
                            'returnPath' => $request->query->get('returnPath', null)
                        ]
                    )
                ));
            }
        } 
        
        return $this->render('ChillBrusafeBundle:Connect:link_confirm.html.twig',
            array(
                'person' => $person,
                'form' => $form->createView()
            ));
    }
    
    /**
     * 
     * @param string $niss
     * @param Person $person
     * @param string $returnPath
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createConfirmForm($niss, Person $person, $returnPath = null)
    {
        return $this->createFormBuilder(
                array('niss' => $niss), 
                array(
                    'action' => $this->generateUrl('chill_brusafe_link_confirm_post',
                        [ 
                            'person_id' => $person->getId(), 
                            'niss' => $niss,
                            'returnPath' => $returnPath
                        ]),
                    'method' => 'POST',
                )
            )
            ->add('code', TextType::class)
            ->add('submit', SubmitType::class)
            ->getForm()
            ;
    }
    
    /**
     * 
     * @param string $action the url the form must redirect
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createPatientForm($action, $niss = null, 
        $returnPath = '')
    {
        return $this->get('form.factory')
            ->createNamedBuilder(
                null, 
                FormType::class, 
                array(
                    'niss' => $niss,
                    'returnPath' => $returnPath
                    ), 
                array(
                    'action' => $action,
                    'method' => 'GET',
                    'csrf_protection' => false,
                )
            )
            ->add('niss', empty($niss) ? TextType::class : HiddenType::class)
            ->add('returnPath', HiddenType::class)
            ->add('submit', SubmitType::class)
            ->getForm()
            ;
    }
}
