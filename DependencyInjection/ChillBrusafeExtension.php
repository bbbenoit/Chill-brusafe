<?php

namespace Chill\BrusafeBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;

/**
 * Configure ChillBrusafe bundle
 */
class ChillBrusafeExtension extends Extension 
    implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services/connector.yml');
        $loader->load('services/form.yml');
        $loader->load('services/generator.yml');
        $loader->load('services/util.yml');
    }
    
    public function prepend(ContainerBuilder $container)
    {
        $this->prependRoutes($container);
    }
    
    public function prependRoutes(ContainerBuilder $container) 
        {
            //add routes for custom bundle
             $container->prependExtensionConfig('chill_main', array(
               'routing' => array(
                  'resources' => array(
                     '@ChillBrusafeBundle/Resources/config/routing.yml'
                  )
               )
            ));
        }
}
