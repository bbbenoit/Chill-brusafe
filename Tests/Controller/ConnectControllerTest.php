<?php

namespace Chill\BrusafeBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ConnectControllerTest extends WebTestCase
{
    public function testConnect()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/{_locale}/brusafe/connect');
    }

    public function testIsconnected()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/{_locale}/brusafe/is_connected');
    }

}
