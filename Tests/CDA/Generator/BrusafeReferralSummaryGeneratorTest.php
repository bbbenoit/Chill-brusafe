<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\BrusafeBundle\Tests\CDA\Generator;

use Chill\BrusafeBundle\CDA\Generator\BrusafeReferralSummaryGenerator;
use Chill\HealthBundle\Entity\Consultation;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Chill\HealthBundle\Entity\Medication;
use Chill\PersonBundle\Entity\Person;
use Chill\HealthBundle\Entity\Publication;
use Chill\MainBundle\Entity\User;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class BrusafeReferralSummaryGeneratorTest extends KernelTestCase
{
    
    /**
     *
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $entityManager;
    
    /**
     *
     * @var BrusafeReferralSummaryGenerator
     */
    protected $generator;


    public function setUp()
    {
        self::bootKernel();
        
        $this->entityManager = self::$kernel
            ->getContainer()
            ->get('doctrine.orm.entity_manager')
            ;
        
        $this->generator = self::$kernel
            ->getContainer()
            ->get('chill_brusafe.abrumet_referral_summary_generator')
            ;
        
        // add a fake request with a default locale (used in translatable string)
        $prophet = new \Prophecy\Prophet;
        $request = $prophet->prophesize();
        $request->willExtend(\Symfony\Component\HttpFoundation\Request::class);
        $request->getLocale()->willReturn('fr');
        
        self::$kernel
            ->getContainer()
            ->get('request_stack')
            ->push($request->reveal());
    }
    
    public function testGenerateXml()
    {
        $publication = $this->createPublication();
        $saml      = $this->createSaml();
        
        $doc = $this->generator->generateXml($publication, $saml);
        
        $doc->formatOutput = true;
        $doc->save(\sys_get_temp_dir().'/cda.xml');
        
        $this->assertContains('ClinicalDocument', $doc->saveXML());
    }
    
    /**
     * @return Publication
     */
    protected function createPublication()
    {
        // get a consultation which contains medication
        $consultations = $this->entityManager
            ->createQuery(
                "SELECT c FROM ChillHealthBundle:Consultation c "
                . "JOIN c.medications m "
                . "WHERE SIZE(c.medications) > 0 "
                )
            ->getResult()
            ;
        
        if (count($consultations) === 0) {
            throw new \RuntimeException("There aren't any consultation available "
                . "to run this test");
        }
        
        /* @var $consultation Consultation */
        $consultation = $consultations[0];
        
        $publication = (new Publication())
            ->setAuthor($consultation->getAuthor())
            ->setPatient($consultation->getPatient())
            ->setDate($consultation->getDate())
            ->setPublicationType('brusafe_referral_summary')
            ;
        
        foreach ($consultation->getMedications() as $medication) {
            $publication->addMedication($medication);
        }
        
        $this->entityManager->persist($publication);
        
        return $publication;
    }
    
    protected function createSaml()
    {
        return array(
            'id_root' => 'test',
            'id_patient' => '1.1234'
        );
    }
}
