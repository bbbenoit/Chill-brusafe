<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\BrusafeBundle\CDA\Generator;

use PHPHealth\CDA\ClinicalDocument;
use PHPHealth\CDA\Elements\Title;
use PHPHealth\CDA\DataType\TextAndMultimedia\CharacterString;
use PHPHealth\CDA\Elements\EffectiveTime;
use PHPHealth\CDA\DataType\Quantity\DateAndTime\TimeStamp;
use PHPHealth\CDA\DataType\Identifier\InstanceIdentifier;
use PHPHealth\CDA\Elements\Id;
use PHPHealth\CDA\Elements\Code;
use PHPHealth\CDA\DataType\Code\LoincCode;
use PHPHealth\CDA\Elements\ConfidentialityCode;
use PHPHealth\CDA\DataType\Code\ConfidentialityCode as ConfidentialityCodeType;
use PHPHealth\CDA\DataType\Code\CodedSimple;
use PHPHealth\CDA\RIM\Participation\Author;
use PHPHealth\CDA\RIM\Role\PatientRole;
use PHPHealth\CDA\RIM\Participation\RecordTarget;
use PHPHealth\CDA\DataType\Name\PersonName;
use Chill\BrusafeBundle\Util\GenderCodeGenerator;
use PHPHealth\CDA\RIM\Entity\Patient;
use PHPHealth\CDA\RIM\Role\AssignedAuthor;
use PHPHealth\CDA\RIM\Entity\AssignedPerson;
use PHPHealth\CDA\RIM\Entity\RepresentedCustodianOrganization;
use PHPHealth\CDA\RIM\Role\AssignedCustodian;
use PHPHealth\CDA\RIM\Participation\Custodian;
use Chill\HealthBundle\Entity\Publication;
use Chill\MainBundle\Entity\User;
use PHPHealth\CDA\DataType\Collection\Set;
use PHPHealth\CDA\DataType\Name\EntityName;
use PHPHealth\CDA\Component\XMLBodyComponent;
use PHPHealth\CDA\Component\SingleComponent\Section;
use PHPHealth\CDA\Component\SingleComponent;
use PHPHealth\CDA\RIM\Act\Observation;
use PHPHealth\CDA\DataType\TextAndMultimedia\NarrativeString;
use PHPHealth\CDA\RIM\Act\SubstanceAdministration;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use PHPHealth\CDA\DataType\Code\StatusCode;
use PHPHealth\CDA\DataType\Collection\Interval\IntervalOfTime;
use PHPHealth\CDA\DataType\Collection\Interval\PeriodicIntervalOfTime;
use Chill\BrusafeBundle\Util\CodeToCDACode;
use PHPHealth\CDA\DataType\Quantity\PhysicalQuantity\PhysicalQuantity;
use PHPHealth\CDA\RIM\Participation\Consumable;
use PHPHealth\CDA\RIM\Role\ManufacturedProduct;
use PHPHealth\CDA\RIM\Entity\ManufacturedLabeledDrug;

/**
 * Generate a CDA "referral summary"
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class BrusafeReferralSummaryGenerator
{
    /**
     * Id root for this document. 
     *
     * @var string
     */
    protected $idRoot = 'https://mass.chill.pro';
    
    protected $custodianName = "Abrumet ASBL";
    protected $custodianId   = '82112744-ea24-11e6-95be-17f96f76d55c';
    
    const TEMPLATE_ID = '1.3.6.1.4.1.19376.1.5.3.1.1.3';
    
    const TITLE = 'Referral summary';
    
    /**
     *
     * @var Publication
     */
    protected $publication;
    
    /**
     *
     * @var \PHPHealth\CDA\Helper\ReferenceManager
     */
    protected $referenceManager;
    
    /**
     *
     * @var \Chill\MainBundle\Templating\TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    /**
     *
     * @var CodeToCDACode
     */
    protected $codeToCda;
    
    
    public function __construct(
        TranslatableStringHelper $translatableStringHelper,
        CodeToCDACode $codeToCDA
    ) {
        $this->translatableStringHelper = $translatableStringHelper;
        $this->codeToCda = $codeToCDA;
    }
    
    /**
     * 
     * @return \DOMDocument
     */
    public function generateXml(Publication $publication, $abrumet)
    {
        $this->publication = $publication;
        $this->abrumet = $abrumet;
        $doc = new ClinicalDocument();
        $this->referenceManager = $doc->getReferenceManager();

        $this->createHeader($doc);
        $this->createBody($doc);
        
        return $doc->toDOMDocument();
    }
    
    protected function getNameFromUser(User $user, $part)
    {
        switch($part) {
            case PersonName::FIRST_NAME : return "Julien";
            case PersonName::LAST_NAME  : return "Fastré";
            case 'suffix'               : return "MD";
        }
    }
    
    protected function createHeader(ClinicalDocument &$doc) 
    {
        $publication = $this->publication;
        
        $doc->setTitle(new Title(new CharacterString(self::TITLE)));
        
        $doc->setEffectiveTime(new EffectiveTime(
                new TimeStamp($publication->getDate())));
        
        $doc->setId(new Id(new InstanceIdentifier(
            '1.2.'.$publication->getId(),
            $this->idRoot.'/health/publication'
            )));
        // add templateId
        $doc->addTtemplateId(new InstanceIdentifier(self::TEMPLATE_ID));
        
        $doc->setCode(new Code(LoincCode::create('42349-1', 'REASON FOR REFERRAL')));
        
        // TODO : get confidentiality code from document
        $doc->setConfidentialityCode(
            new ConfidentialityCode(
                ConfidentialityCodeType::create(
                    ConfidentialityCodeType::RESTRICTED_KEY, 
                    ConfidentialityCodeType::RESTRICTED
                    )
                )
            );
        
        // TODO: get document language dynamically
        $doc->setLanguageCode(new CodedSimple('fr_FR'));
        
        $doc->setRecordTarget(
            $this->getRecordTarget()
        );
        
        $doc->setAuthor(new Author(
            new TimeStamp($publication->getDate()), 
            $this->getAssignedAuthor()
            ));
        $doc->setCustodian($this->getCustodian());
    }
    
    protected function getRecordTarget() {
        $person = $this->publication->getPatient();
        
        $pr = new PatientRole(
            $this->getPatientsIds(), 
            $this->getPatient());
        
        return new RecordTarget($pr);
    }
    
    protected function getPatientsIds() 
    {
        $person  = $this->publication->getPatient();
        $abrumet = $this->abrumet;
        
        $set = new Set(InstanceIdentifier::class);
        
        $set->add(new InstanceIdentifier('1.1'.$person->getId(), $this->idRoot.'/person/person'));
        $set->add(new InstanceIdentifier($abrumet['id_patient'], $abrumet['id_root']));
        
        return $set;
    }
    
    protected function getPatient()
    {
        $person = $this->publication->getPatient();
        
        $names = new Set(PersonName::class);
        $names->add(
            (new PersonName())
                ->addPart(PersonName::FIRST_NAME, $person->getFirstName())
                ->addPart(PersonName::LAST_NAME, $person->getLastName())
                );
        $patient = new Patient(
                $names, 
                new TimeStamp($person->getBirthdate()),
                GenderCodeGenerator::generate($person)
                );
        
        return $patient;
    }
    
    /**
     * 
     * @return AssignedAuthor
     */
    protected function getAssignedAuthor()
    {
        $names = new Set(PersonName::class);
        
        $names->add((new PersonName())
            ->addPart(PersonName::FIRST_NAME, 
                $this->getNameFromUser(
                    $this->publication->getAuthor(), 
                    PersonName::FIRST_NAME
                    ))
            ->addPart(PersonName::LAST_NAME, 
                $this->getNameFromUser(
                    $this->publication->getAuthor(),
                    PersonName::LAST_NAME
                    ))
            ->addPart('suffix', 
                $this->getNameFromUser(
                    $this->publication->getAuthor(), 
                    'suffix'
                    ))
            );
        
        $assignedAuthor = new AssignedAuthor(
            new AssignedPerson($names),
            (new Set(InstanceIdentifier::class))
                ->add(new InstanceIdentifier( 
                    '1.2.'.$this->publication->getAuthor()->getId(),
                    $this->idRoot.'/user'
                    ))
            );
        
        return $assignedAuthor;
    }
    
    /**
     * 
     * @return Custodian
     */
    protected function getCustodian()
    {
        $names = (new Set(EntityName::class))
            ->add(new EntityName($this->custodianName));
        $ids = (new Set(InstanceIdentifier::class))
            ->add(new InstanceIdentifier(\strtoupper($this->custodianId)));
        
        $reprCustodian = new RepresentedCustodianOrganization($names, $ids);
        
        $assignedCustodian = new AssignedCustodian($reprCustodian);
        
        return new Custodian($assignedCustodian);
    }
    
    
    protected function createBody(ClinicalDocument $document)
    {
        $publication = $this->publication;
        $xmlBody = new XMLBodyComponent();
        
        // create the first components
        $components = array(
            [
                '1.3.6.1.4.1.19376.1.5.3.1.3.1', // templateId of section
                new InstanceIdentifier('1.2.'.$publication->getId().'.0', 
                    $this->idRoot.'/health/publication/section'), //id of section
                new LoincCode('42349-1', 'Reason for referral'), // code for section
                "", // text for section
                array() // no acts
            ],
            [
                '1.3.6.1.4.1.19376.1.5.3.1.3.4', // templateId of section
                new InstanceIdentifier('1.2.'.$publication->getId().'.1', 
                    $this->idRoot.'/health/publication/section'), //id of section
                new LoincCode('10164-2', 'History of Present Illness Section'), // code for section
                "No statement.", // text for section
                array() // no acts
            ],
            [
                '1.3.6.1.4.1.19376.1.5.3.1.3.6', // templateId of section
                new InstanceIdentifier('1.2.'.$publication->getId().'.2', 
                    $this->idRoot.'/health/publication/section'), //id of section
                new LoincCode('11450-4', 'Active Problems Section'), // code for section
                "No statement.", // text for section
                array(Observation::createNullObservation())
            ]
            );
        
        foreach ($components as list($templateId, $id, $loinc, $text, $acts)) {
            $component = $this->createSection($templateId, $id, $loinc, $text, 
                $acts);
            $xmlBody->addComponent($component);
        }
        
        // add medication section
        $xmlBody->addComponent($this->createComponentMedications());
        
        // add allergies
        $xmlBody->addComponent($this->createSection(
                '1.3.6.1.4.1.19376.1.5.3.1.3.13', // templateId of section
                new InstanceIdentifier('1.2.'.$publication->getId().'.4', 
                    $this->idRoot.'/health/publication/section'), //id of section
                new LoincCode('48765-2', 'allergies and Other Adverse Reactions Section'), // code for section
                "No statement.", // text for section
                array(Observation::createNullObservation()) 
            ));
            
        $document->getRootComponent()->addComponent($xmlBody);
    }
    
    /**
     * Create a Single component and a section, and fill it with 
     * required elements
     * 
     * @param string $templateId
     * @param InstanceIdentifier $id
     * @param LoincCode $loinc
     * @param string $text
     * @param array $acts
     * @return SingleComponent
     */
    protected function createSection($templateId, InstanceIdentifier $id, $loinc, $text, $acts)
    {
        $section = (new Section())
                ->addTemplateId(new InstanceIdentifier($templateId))
                ->setTitle(new CharacterString($loinc->getDisplayName()))
                ->setId($id)
                ->setCode($loinc)
                ->setText(
                    // if is a string, create a character string,
                    // else, assume that `$text` implements AnyType
                    is_string($text) ? new CharacterString($text) : $text
                    )
                ;
            
            foreach ($acts as $act) {
                $entry = $section->createEntry();
                $entry->addAct($act);
            }

            $component = (new SingleComponent())
                ->addSection($section);
            
            return $component;
    }

    /**
     * Create a section "medication"
     */
    public function createComponentMedications()
    {
        $table = (new NarrativeString())
            ->createTable()
                ->getThead()
                    ->createRow()
                        ->createCell('Date from')->getRow()
                        ->createCell('Substance')->getRow()
                        ->createCell('Dosage')->getRow()
                        ->createCell('Administration frequency')->getRow()
                        ->createCell('Date to')->getRow()
                        ->createCell('Instructions')->getRow()
                    ->getSection()
                ->getTable();
        $acts = array();
        
        foreach ($this->publication->getMedications()->toArray() as $medication) {
            /* @var $medication \Chill\HealthBundle\Entity\Medication */
            
            $id = $medication->getId();
            $table->getTbody()
                ->createRow()
                    // set a reference to this row
                    ->setReference(
                        $this->referenceManager
                            ->getReferenceType('medication_'.$id)
                        )
                    // date from
                    ->createCell(
                        $medication->getDateFrom()->format(\DateTime::ISO8601)
                        )
                        ->setReference(
                            $this->referenceManager
                                ->getReferenceType('medication_date_from_'.$id)
                            )
                        ->getRow()
                    ->createCell(
                        $this->translatableStringHelper->localize(
                            $medication->getSubstance()->getDisplayName()
                            ))
                        ->setReference(
                            $this->referenceManager
                                ->getReferenceType('medication_substance_'.$id)
                            )
                        ->getRow()
                    ->createCell(
                            $medication->getDoseQuantityValue()
                            ." "
                            .$medication->getDoseQuantityUnit()
                        )
                        ->setReference(
                            $this->referenceManager
                                ->getReferenceType('medication_dosage_'.$id)
                            )
                        ->getRow()
                    ->createCell(
                            $medication->getAdministrationFrequency()
                        )
                        ->setReference(
                            $this->referenceManager
                                ->getReferenceType('medication_administration_'
                                    . 'frequency_'.$id)
                            )
                        ->getRow()
                    ->createCell(
                            $medication->getDateTo()->format(\DateTime::ISO8601)
                        )
                        ->setReference(
                            $this->referenceManager
                                ->getReferenceType('medication_date_to_'.$id)
                            )
                        ->getRow()
                    ->createCell(
                            empty($medication->getModeDelivrance())
                            ?
                            ''
                            :
                            $medication->getModeDelivrance()
                        )
                        ->setReference(
                            $this->referenceManager
                                ->getReferenceType('medication_mode_deliv_'.$id)
                            )
                        ->getRow()
            ;
            
            $acts[] = (new SubstanceAdministration)
                ->setTemplateIds(array(
                    new InstanceIdentifier('2.16.840.1.113883.10.20.1.24'),
                    new InstanceIdentifier('1.3.6.1.4.1.19376.1.5.3.1.4.7')
                ))
                ->setIds(
                    (new Set(InstanceIdentifier::class))
                        ->add(new InstanceIdentifier('1.4.'.$id, 
                            $this->idRoot.'/health/medication'))
                    )
                ->setText($this->referenceManager
                    ->getReferenceElement('medication_'.$id)
                    )
                ->setStatusCode(
                    new StatusCode(StatusCode::COMPLETED))
                ->setEffectiveTime(
                    new IntervalOfTime(
                        new TimeStamp($medication->getDateFrom()), 
                        new TimeStamp($medication->getDateTo())
                        )
                    )
                // TODO: set correct effective time
                ->setEffectiveTime(
                    (new PeriodicIntervalOfTime(new \DateInterval('PT12H')))
                        ->setInstitutionSpecified(true)
                    )
                ->setRouteCode($this->codeToCda
                    ->toCode($medication->getRouteOfAdministration()))
                ->setDoseQuantity(
                    new PhysicalQuantity(
                        $medication->getDoseQuantityUnit(), 
                        $medication->getDoseQuantityValue()
                        )
                    )
                ->setConsumable(
                    new Consumable(
                        new ManufacturedProduct(
                            new ManufacturedLabeledDrug(
                                $this->codeToCda->toCode($medication->getSubstance())
                                )
                            )
                        )
                    )
                ;
            }
            
        return $this->createSection(
            '1.3.6.1.4.1.19376.1.5.3.1.3.19', 
            new InstanceIdentifier('1.2.'.$this->publication->getId().'.3', 
                $this->idRoot.'/health/publication/section'), 
            new LoincCode('10160-0', 'Medication Sections'), 
            $table->getNarrative(), 
            $acts);
    }
}
